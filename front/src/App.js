import './App.scss';

import {QueryClient, QueryClientProvider} from '@tanstack/react-query';
import Home from "./components/pages/home";
import {BrowserRouter, Route, Routes} from "react-router-dom";
import Header from "./components/header";
import Footer from "./components/footer";
import Create from "./components/pages/create";
import ArticleView from "./components/pages/articleView";

const queryClient = new QueryClient();

function App() {
  return (
    <>
        <QueryClientProvider client={queryClient}>
            <Header/>

            <BrowserRouter>
                <Routes>
                    <Route path="/" element={<Home/>} />
                    <Route path="create" element={<Create/>} />
                    <Route path="*" element={<ArticleView/>} />
                </Routes>
            </BrowserRouter>

            <Footer/>
        </QueryClientProvider>
    </>
  );
}

export default App;
