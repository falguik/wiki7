import {Link} from "react-router-dom";

function NotFound() {
    return (
        <main>
            <section className="no-results-section">
                <h2>Aucun article trouvé</h2>
                <p>Il semble que nous n'ayons pas d'article correspondant à votre recherche.</p>
                <Link to="/create" className="button">Rédiger un nouvel article</Link>
            </section>
        </main>
    );
}

export default NotFound;