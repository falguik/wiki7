import {useLocation} from "react-router-dom";
import {useQuery} from "@tanstack/react-query";
import NotFound from "../notFound";

function ArticleView() {
    const location = useLocation();
    const validRoute = slugify(location.pathname.substring(1));

    console.log(validRoute)

    const { data, error, isLoading } = useQuery({queryFn: () => fetchArticles(validRoute),
        queryKey: ['articles', validRoute]});

    if(isLoading) {
        return <p>Recherche de l'article en cours...</p>
    }

    if (error) {
        return <NotFound/>
    }

    return (
        <main>
            <article className="article-content">
                <h1 className="font-bold text-5xl mb-5">{data.title}</h1>

                <div className="tags mb-2">
                    {data.tags && data.tags.split(',').map((element) => <span>{element}</span> )}
                </div>

                <h2 className="text-3xl mb-3">{data.subTitle}</h2>
                <p className="whitespace-pre-wrap">{data.content}</p>
            </article>
        </main>
    );
}

async function fetchArticles(param) {
    return (await fetch('http://localhost:3001/article/route/' + param)).json();
}

function slugify(text) {
    return text
        .toString()
        .toLowerCase()
        .trim()
        .replace(/\s+/g, '-')
        .replace(/&/g, '-and-')
        .replace(/[^\w\-]+/g, '')
        .replace(/\-\-+/g, '-');
}

export default ArticleView;