import {useQuery} from "@tanstack/react-query";
import Error from "../error";
import Article from "../article";
import {Link} from "react-router-dom";

function Home() {
    const { data, error, isLoading } = useQuery({queryFn: fetchArticles, queryKey: ['articles']});

    if (error) {
        return <Error message={error.message}/>
    }

    return (
        <>
            <main>
                <section className="write-article-ad">
                    <p>Rédiger un article</p>
                    <Link to="/create" className="button">Commencer à écrire</Link>
                </section>

                <h1 className="text-center text-[#333] my-4 font-bold text-2xl">Articles Récents</h1>

                <section className="recent-articles">
                    {!isLoading ? data.articles.map((element, index) => (
                        <Article key={index} article={element}/>
                    )) : ""}
                </section>
            </main>
        </>
    );
}

async function fetchArticles() {
    return (await fetch('http://localhost:3001/article')).json();
}


export default Home;