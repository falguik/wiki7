import {useMutation} from "@tanstack/react-query";
import {useNavigate} from "react-router-dom";

function Create() {
    const navigate = useNavigate();

    const mutation = useMutation({mutationFn: submitArticle,
    onSuccess: () => navigate('/'), onError: (error) => console.log(error)})

    const handleSubmit = (event) => {
        event.preventDefault();
        const formData = new FormData(event.target);
        const articleData = Object.fromEntries(formData.entries());
        mutation.mutate(articleData);
    };

    return (
        <main>
            <section className="article-editor">
                <h1>Rédiger un Nouvel Article</h1>
                <form onSubmit={handleSubmit}>
                    <div className="form-group">
                        <label htmlFor="title">Titre de l'Article</label>
                        <input type="text" id="title" name="title"></input>
                    </div>

                    <div className="form-group">
                        <label htmlFor="subTitle">Sous-Titre</label>
                        <input type="text" id="subTitle" name="subTitle"></input>
                    </div>

                    <div className="form-group">
                        <label htmlFor="content">Contenu</label>
                        <textarea id="content" name="content"></textarea>
                    </div>

                    <div className="form-group">
                        <label htmlFor="tags">Tags (séparés par des virgules)</label>
                        <input type="text" id="tags" name="tags"></input>
                    </div>

                    <button type="submit">Publier l'Article</button>
                </form>
            </section>
        </main>
    );
}

async function submitArticle(data) {
    const response = await fetch('http://localhost:3001/article/publish', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(data)
    });

    return response.json();
}

export default Create;