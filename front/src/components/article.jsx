import {useNavigate} from "react-router-dom";

function Article(props) {
    const { article } = props;
    const navigate = useNavigate();

    const handleClick = (route) => {
        navigate('/' + route);
    };

    return (
        <article onClick={() => handleClick(article.route)}>
            <h2 className="font-bold">{article.title}</h2>
            <h3>{article.subTitle}</h3>
            <p>{truncate(article.content, 20)}</p>

            <div className="tags">
                {article.tags && article.tags.split(',').map((element) => <span>{element}</span> )}
            </div>
        </article>
    );
}

function truncate(data, max) {
    if (data.length > max) {
        return data.substring(0, max) + '...';
    } else {
        return data;
    }
}

export default Article;