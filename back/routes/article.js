const express = require('express');
const articleService = require('../services/articleService');
const validateMiddleware = require('../middlewares/validateMiddleware');

const router = express.Router();

router.get('/', async (req, res) => {
    res.send({articles: await articleService.getAll()})
})

router.get('/id/:id', async (req, res) => {
    res.send(await articleService.getById(parseInt(req.params.id)))
})

router.get('/route/:route', async (req, res) => {
    res.send(await articleService.getByRoute(req.params.route))
})

router.post('/publish', validateMiddleware.validateArticle(), async (req, res) => {
    res.send(await articleService.publish(req.body))
})

module.exports = router;