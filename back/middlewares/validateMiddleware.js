const {z} = require("zod");

function validateArticle() {
    return (req, res, next) => {
        try {
            articleSchema.parse(req.body);
            next();
        } catch (e) {
            return res.status(400).json({ error: e.errors, message: "Invalid schema" });
        }
    };
}

const articleSchema = z.object({
    title: z.string().min(1),
    subTitle: z.string().min(1).optional(),
    content: z.string().min(1),
    tags: z.string().optional(),
});

module.exports = {
    validateArticle,
};