const prisma = require('../database/prismaClient');

function test() {
    return {message: 'Hello !'}
}

async function getAll() {
    return prisma.article.findMany();
}

async function getById(id) {
    return prisma.article.findFirst({
        where: {
            id
        }
    })
}

async function getByRoute(route) {
    return prisma.article.findFirst({
        where: {
            route
        }
    })
}

async function publish(article) {
    return prisma.article.create({
        data: {
            route: slugify(article.title),
            ...article
        }
    })
}

function slugify(text) {
    return text
        .toString()
        .toLowerCase()
        .trim()
        .replace(/\s+/g, '-')
        .replace(/&/g, '-and-')
        .replace(/[^\w\-]+/g, '')
        .replace(/\-\-+/g, '-');
}


module.exports = {
    test,
    getAll,
    getById,
    getByRoute,
    publish,
};