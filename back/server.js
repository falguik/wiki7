const express = require('express');
const bodyParser = require('body-parser');
const prisma = require('./database/prismaClient');
const cors = require('cors');

const articleRoutes = require('./routes/article');

const port = 3001
const app = express();
app.use(bodyParser.json());

const corsOptions = {
    origin: '*',
    methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
    preflightContinue: false,
    optionsSuccessStatus: 204
};

app.use(cors(corsOptions));

app.use('/article', articleRoutes);


// End mysql connection when api turn off
process.on('SIGINT', async () => {
    await prisma.$disconnect();
    process.exit();
});

// Start api
app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
})
